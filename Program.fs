﻿// Learn more about F# at http://fsharp.org
open System
open System.Text
open Metro

// 文字化け対策
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

// 目的: ダイクストラアルゴリズムメイン関数
// Eki listとEkikan listを入力されたら各駅について最短距離と最短経路を入れたEki listを返す
// dijkstraMain: Eki list -> Ekikan list -> Eki list
let rec dijkstraMain ekikanList ekiList =
    match ekiList with
    | [] -> []
    | first :: rest ->
        let (p, v) = saitanWoBunri (first :: rest)
        let ekiList2 = koushin p v ekikanList
        p :: dijkstraMain ekikanList ekiList2

let dijkstra kiten shuten =
    // 最後の絞込のときにだけ使う変数
    let shutenKanji = (romajiToKanji shuten globalEkimeiList)

    romajiToKanji kiten globalEkimeiList   // 基点の漢字を作って、
    |> makeInitialEkiList globalEkimeiList // 初期リストを作成し、
    |> dijkstraMain globalEkikanList       // ダイクストラ法で最短距離を求める 
    |> List.find (fun retElem -> retElem.Namae = shutenKanji) // ダイクストラ法の結果から終点のものを絞り込む

[<EntryPoint>]
let main argv =
    printfn "ダイクストラアルゴリズムの結果リスト表示"
    printfn "%A" (dijkstra "myogadani" "meguro")
    0 // return an integer exit code

