// 再利用用判別式関数
let hanbetsushiki a b c = b ** 2.0 - 4.0 * a * c

// 目的：判別式を使って2次方程式の解の個数を出力する
//       但し、a,b,c∈R,a≠0とする
// kaiNoKosuu : float -> float -> float -> int *
let kaiNoKosuu a b c = 
    if hanbetsushiki a b c < 0.0 then 0
        else if hanbetsushiki a b c = 0.0 then 1
        else if hanbetsushiki a b c > 0.0 then 2
        else -1

// テスト 
let test1 = kaiNoKosuu 1.2 2.87 3.9 = 0
let test2 = kaiNoKosuu 3.4 298.3 18.78 = 2
let test3 = kaiNoKosuu 1.0 4.0 4.0 = 1

printfn "結果の表示" 
printfn "対象の関数: %A" kaiNoKosuu
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""
