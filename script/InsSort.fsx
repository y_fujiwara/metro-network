(* int list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が int で残りのリストが rest
                     (firstはint型 restが自己参照のケース)
*)

// 再利用用Insert関数
// insert : int -> int list -> inr list 
let rec insert list n = 
    match list with
    | first :: rest when n < first -> n :: first :: rest
    | first :: rest -> first :: insert rest n
    |  [] -> [n;]

// 目的：挿入ソートによって引数で与えられたリストをソートする
// insSort: int list -> int list
let rec insSort list = 
    // list2を整列済みリストとして本関数を再帰する
    // list1が空になったタイミングがリストを舐め終わったタイミングなのでlist2が整列済み
    let rec insInner list1 list2 =
        match list1 with
          | [] -> list2 // list1が空の場合はターゲットのリストは確認済み
          | inFirst :: inRest
              -> insInner inRest (insert list2 inFirst) // 整列済みリストに対象のデータをInsertして再帰する
    // 初期値は整列済みリストは空
    insInner list []

let test1 = insSort [7; 4; 3; 8; 1; 5;] = [1; 3; 4; 5; 7; 8;]
let test2 = List.isEmpty (insSort [])
let test3 = insSort [4; 1; 5; 10; 8; 3; 7;] = [1; 3; 4; 5; 7; 8; 10;]
let test4 = insSort [7; 3; 8; 4; 1; 0;] = [0; 1; 3; 4; 7; 8;]

printfn "結果の表示" 
printfn "対象の関数: %A" insSort
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn ""
