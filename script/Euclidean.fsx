// 目的: ２つの自然数nとmを入力されたときに２つの最大公約数を求める
// n,mはともに自然数なのでmod計算を行うことで必ず0に帰着する(0以下にはならないし増加もしない)
// gcd: int -> int -> int
let rec gcd m n =
    if n = 0
    then m
    else gcd n (m % n)

// テスト
let test1 = gcd 0 0 = 0
let test2 = gcd 100 0 = 100
let test3 = gcd 30 42 = 6
let test4 = gcd 630 300 = 30

printfn "結果の表示" 
printfn "対象の関数: %A" gcd
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn ""


