(* int list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が int で残りのリストが rest
                     (firstはint型 restが自己参照のケース)
*)

// 目的：数字と整列済みリストを入力し昇順の位置に数字を挿入する
// insert : int -> int list -> inr list *
let rec insert list n = 
    match list with
    | first :: rest when n < first -> n :: first :: rest 
    | first :: rest -> first :: insert rest n
    |  [] -> [n;]
let test1 = insert [1; 3; 4; 7; 8;] 5 = [1; 3; 4; 5; 7; 8;]
let test2 = insert [] 5 = [5;]
let test3 = insert [1; 3; 4; 7; 8;] 10 = [1; 3; 4; 7; 8; 10;]
let test4 = insert [1; 3; 4; 7; 8;] 0 = [0; 1; 3; 4; 7; 8;]

printfn "結果の表示" 
printfn "対象の関数: %A" insert
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn ""
