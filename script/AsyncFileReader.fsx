open System.IO
let openFile fileName =
    use fs = new FileStream(fileName, FileMode.Open)
    let data = Array.create (int fs.Length) 0uy
    let callback ar =
        fs.EndRead(ar) |> ignore
        ar.AsyncWaitHandle.Close()
    let asyncResult =        fs.BeginRead(data, 0, data.Length, (fun ar -> callback ar), null)
    asyncResult.AsyncWaitHandle.WaitOne() |> ignore
    data