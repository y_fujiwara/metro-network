// 目的: 入力された2以上n以下の自然数のリストから素数をすべて求める
// List.filterによって部分問題は必ずより小さいリストを処理することになるので停止する
// sieve: int list -> int list
let rec sieve list =
    match list with
    | [] -> []
    | first :: rest -> first :: sieve (List.filter (fun n -> n % first <> 0) rest)

// 目的: 自然数nを受け取りそれ以下の素数のリストを求める
// prime: int -> int list
let prime n = sieve [2..n]

// テスト
let test1 = sieve [2] = [2]
let test2 = sieve [2..30] = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29]
let test3 = sieve [2..300] = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29; 31; 37; 
    41; 43; 47; 53; 59; 61; 67; 71; 73; 79; 83; 89; 97; 101; 103; 107; 
    109; 113; 127; 131; 137; 139; 149; 151; 157; 163; 167; 173; 179; 181; 
    191; 193; 197; 199; 211; 223; 227; 229; 233; 239; 241; 251; 257; 263; 
    269; 271; 277; 281; 283; 293;
]
let test11 = prime 2 = [2]
let test12 = prime 30 = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29]
let test13 = prime 300 = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29; 31; 37; 
    41; 43; 47; 53; 59; 61; 67; 71; 73; 79; 83; 89; 97; 101; 103; 107; 
    109; 113; 127; 131; 137; 139; 149; 151; 157; 163; 167; 173; 179; 181; 
    191; 193; 197; 199; 211; 223; 227; 229; 233; 239; 241; 251; 257; 263; 
    269; 271; 277; 281; 283; 293;
]


printfn "結果の表示" 
printfn "対象の関数: %A" sieve
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""

printfn "結果の表示(prime)"
printfn "対象の関数: %A" prime
printfn "test1: %b" test11
printfn "test2: %b" test12
printfn "test3: %b" test13
printfn ""



