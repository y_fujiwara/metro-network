(* PersonT list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはPersonT型 restが自己参照のケース)
*)

// 各人のデータ (名前, 身長m, 体重kg, 誕生日月日, 血液型) を表す型
type PersonT = {
    Name: string;      // 名前
    Sintyou: float;    // 身長
    Taijyuu: float;    // 体重
    Birthday: string;  // 誕生日
    BloodType: string; // 血液型
}

// PersonT型のデータの例
let lst1 = []
let lst2 = [
    {Name = "fujiwara"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/06/01"; BloodType = "A";};
    ]
let lst3 = [
    {Name = "yoshiaki"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/08/30"; BloodType = "B";};
    {Name = "fujiwara"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/06/01"; BloodType = "A";};
    ]
let lst4 = [
    {Name = "aaa"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/06/01"; BloodType = "A";};
    {Name = "111"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/12/30"; BloodType = "B";};
    {Name = "ddd"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/9/20"; BloodType = "A";};
    ]
let lst5 = [
    {Name = "abc"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/8/23"; BloodType = "A";};
    {Name = "cde"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/9/22"; BloodType = "A";}; 
    {Name = "bcd"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/8/22"; BloodType = "B";};
    {Name = "aaa"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/9/23"; BloodType = "AB";}; 
    ]

// 目的: 与えられたリストをNameフィールドによって整列したリストを返す
// pertonSort : PersonT list -> PersonT list
let personSort list = 
    let rec insert list n = 
        match list with
        | first :: rest when n.Name < first.Name -> n :: first :: rest
        | first :: rest -> first :: insert rest n
        |  [] -> [n;]

    let rec insInner list1 list2 =
        match list1 with
          | [] -> list2
          | inFirst :: inRest
              -> insInner inRest (insert list2 inFirst) // 整列済みリストに対象のデータをInsertして再帰する
    insInner list []

// テスト
let test1 = List.isEmpty (personSort lst1)
let test2 =
    personSort lst2 = [
        {Name = "fujiwara"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/06/01"; BloodType = "A";}; 
        ]

let test3 = 
    personSort lst3 = [
        {Name = "fujiwara"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/06/01"; BloodType = "A";}; 
        {Name = "yoshiaki"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/08/30"; BloodType = "B";}; 
        ]

let test4 = 
    personSort lst4 = [
        {Name = "111"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/12/30"; BloodType = "B";};
        {Name = "aaa"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/06/01"; BloodType = "A";};
        {Name = "ddd"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/9/20"; BloodType = "A";};
        ]

let test5 = 
    personSort lst5 = [
        {Name = "aaa"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/9/23"; BloodType = "AB";}; 
        {Name = "abc"; Sintyou = 1.70; Taijyuu = 67.8; Birthday = "1990/8/23"; BloodType = "A";};
        {Name = "bcd"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/8/22"; BloodType = "B";};
        {Name = "cde"; Sintyou = 2.10; Taijyuu = 90.4; Birthday = "2000/9/22"; BloodType = "A";}; 
        ]




printfn "結果の表示" 
printfn "対象の関数: %A" personSort
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn ""

