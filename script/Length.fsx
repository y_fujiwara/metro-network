(* int list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはint型 restが自己参照のケース)
*)

// 目的: 受け取ったリストの要素数を求める
// length : int list -> int
let rec length list =
    match list with
        [] -> 0
      | first :: rest -> 1 + length rest // 末尾先になってないので重い

// テスト
let test1 = length [] = 0
let test2 = length [1;] = 1
let test3 = length [1; 2;] = 2
let test4 = length [2; 1; 6; 4; 7;] = 5
let test5 = length (1 :: []) = 1
let test6 = length (1 :: 2 :: []) = 2


printfn "結果の表示" 
printfn "対象の関数: %A" length
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn "test6: %b" test6
printfn ""
