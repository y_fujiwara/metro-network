(* GakuseiT list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはGakuseiT型 restが自己参照のケース)
*)

// 各学生のデータ (名前, 点数, 成績) を表す型
type GakuseiT = { 
    Namae : string;   // 学生の名前 
    Tensuu : int;     // 点数 
    Seiseki : string; // 成績
}

let gakusei1  = {Namae="asai"; Tensuu=70; Seiseki="?"}
let gakusei1' = {Namae="asai"; Tensuu=70; Seiseki="B"}
let gakusei2  = {Namae="tanaka"; Tensuu=90; Seiseki="?"}
let gakusei2' = {Namae="tanaka"; Tensuu=90; Seiseki="A"}
let gakusei3  = {Namae="yamada"; Tensuu=60; Seiseki="?"}
let gakusei3' = {Namae="yamada"; Tensuu=60; Seiseki="C"}
let gakusei4  = {Namae="yoshida"; Tensuu=80; Seiseki="?"}
let gakusei4' = {Namae="yoshida"; Tensuu=80; Seiseki="A"}
let gakusei5  = {Namae="kimura"; Tensuu=85; Seiseki="?"}
let gakusei5' = {Namae="kimura"; Tensuu=85; Seiseki="A"}
let gakusei6  = {Namae="kudo"; Tensuu=65; Seiseki="?"}
let gakusei6' = {Namae="kudo"; Tensuu=65; Seiseki="C"}
let gakusei7  = {Namae="ito"; Tensuu=75; Seiseki="?"}
let gakusei7' = {Namae="ito"; Tensuu=75; Seiseki="B"}

// 学生のリスト
let gakuseiList  = [
    gakusei1; gakusei2; gakusei3; gakusei4; gakusei5; gakusei6; gakusei7
    ]

let gakuseiList' = [
    gakusei1'; gakusei2'; gakusei3'; gakusei4'; gakusei5'; gakusei6'; gakusei7'
    ]

// 目的: 与えられたリストをTennsuuフィールドによって整列したリストを返す
// gakuseiSort: GakuseiT list -> GakuseiT list
let gakuseiSort list = 
    let rec insert list n = 
        match list with
        | first :: rest when n.Tensuu < first.Tensuu -> n :: first :: rest
        | first :: rest -> first :: insert rest n
        |  [] -> [n;]

    let rec insInner list1 list2 =
        match list1 with
          | [] -> list2
          | inFirst :: inRest
              -> insInner inRest (insert list2 inFirst) // 整列済みリストに対象のデータをInsertして再帰する
    insInner list []

// テスト
let test1 = gakuseiSort gakuseiList = [gakusei3; gakusei6; gakusei1; gakusei7; gakusei4; gakusei5; gakusei2;]
let test2 = gakuseiSort [gakusei1;] = [gakusei1;]
let test3 = gakuseiSort [] = []


printfn "結果の表示" 
printfn "対象の関数: %A" gakuseiSort
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test2: %b" test3
printfn ""



