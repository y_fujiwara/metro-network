// 各人のデータ (名前, 身長m, 体重kg, 誕生日月日, 血液型) を表す型
type PersonT = {
    name: string;      // 名前
    sintyou: float;    // 身長
    taijyuu: float;    // 体重
    birthday: string;  // 誕生日
    bloodType: string; // 血液型
}

// 目的：入力されたpersonT型データについて
//      「nameさんの血液型はbloodTypeです」の文字列を返す
// ketsuekiHyouji : PersonT -> string *
let ketsuekiHyouji person = 
  match person with
    {name = n; sintyou = m; taijyuu = kg; birthday = day; bloodType = blood} ->
      n + "さんの血液型は" + blood + "型です"

// テスト 
let test1 = ketsuekiHyouji {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";}
                         = "fujiwaraさんの血液型はA型です"

let test2 = ketsuekiHyouji {name = "yoshiaki"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/12/30"; bloodType = "B";}
                         = "yoshiakiさんの血液型はB型です"

printfn "結果の表示" 
printfn "対象の関数: %A" ketsuekiHyouji
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn ""



