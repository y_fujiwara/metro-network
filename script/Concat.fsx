(* string list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはstring型 restが自己参照のケース)
*)

// 目的: 受け取った文字列リストを結合して一つの文字列にする
// concat : string list -> string
let rec concat list =
    match list with
        [] -> ""
      | first :: rest -> first + concat rest

// テスト
let test1 = concat [] = ""
let test2 = concat ["A";] = "A"
let test3 = concat ["春"; "夏"; "秋"; "冬";] = "春夏秋冬"
let test4 = concat ["プ"; "ロ"; "グ"; "ラ"; "ム";] = "プログラム"
let test5 = concat ("B" :: []) = "B"
let test6 = concat ("A" :: "B" :: []) = "AB"


printfn "結果の表示" 
printfn "対象の関数: %A" concat
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn "test6: %b" test6
printfn ""
