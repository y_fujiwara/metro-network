// 目的：月と日を受け取ったら星座を返す 不正な月日に関しては関知しない
// seiza : int -> int -> string *
let seiza mm dd = 
    if (mm = 3 && dd >= 21) || (mm = 4 && dd <= 19) then "Aries"
        else if (mm = 4 && dd >= 20) || (mm = 5 && dd <= 20) then "Taurus"
        else if (mm = 5 && dd >= 21) || (mm = 6 && dd <= 21) then "Gemini"
        else if (mm = 6 && dd >= 22) || (mm = 7 && dd <= 22) then "Leo"
        else if (mm = 8 && dd >= 23) || (mm = 9 && dd <= 22) then "Virgo"
        else if (mm = 9 && dd >= 23) || (mm = 10 && dd <= 23) then "Libra"
        else if (mm = 10 && dd >= 24) || (mm = 11 && dd <= 22) then "Scorpio"
        else if (mm = 11 && dd >= 23) || (mm = 12 && dd <= 21) then "Sagittarius"
        else if (mm = 12 && dd >= 22) || (mm = 1 && dd <= 19) then "Capricorn"
        else if (mm = 1 && dd >= 20) || (mm = 2 && dd <= 18) then "Aquarius"
        else if (mm = 2 && dd >= 19) || (mm = 3 && dd <= 20) then "Pisces"
        else ""

// テスト
let test1 = seiza 6 1 = "Gemini"
let test2 = seiza 3 22 = "Aries"
let test3 = seiza 2 27 = "Pisces"

printfn "結果の表示" 
printfn "対象の関数: %A" seiza
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""