// 目的：受け取った項数nに対して数列a_nの値を求める
// zenkashiki : int -> int
let rec zenkashiki n = 
    if n = 0 then 3 else 2 * zenkashiki (n - 1) - 1

let test1 = zenkashiki 0 = 3
let test2 = zenkashiki 1 = 5
let test3 = zenkashiki 2 = 9
let test4 = zenkashiki 3 = 17

printfn "結果の表示" 
printfn "対象の関数: %A" zenkashiki
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn ""


