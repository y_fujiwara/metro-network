// 目的: 入力された自然数リストからそれまでの数の合計からなるリストを作る
// sumList: int list -> int list
let sumList list =
    let rec sumAccum list total =
        match list with
        | [] -> []
        | first :: rest -> (first + total) :: sumAccum rest (first + total)
    sumAccum list 0

// テスト
let test1 = List.isEmpty (sumList [])
let test2 = sumList [1] = [1]
let test3 = sumList [3; 2; 1; 4] = [3; 5; 6; 10]


printfn "結果の表示" 
printfn "対象の関数: %A" sumList
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test2: %b" test3
printfn ""

