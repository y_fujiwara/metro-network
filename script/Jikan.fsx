// 目的：入力された時間が午前か午後かを判断して返す
// jikan : int -> int -> string *
let jikan mm dd = 
    if (mm % 24 + dd / 60) < 12 then "AM" else "PM"

// テスト
let test1 = jikan 10 20 = "AM"
let test2 = jikan 13 00 = "PM"
let test3 = jikan 25 90 = "AM"

printfn "結果の表示" 
printfn "対象の関数: %A" jikan
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""

