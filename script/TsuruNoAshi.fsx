// 目的：入力された鶴の数に応じて足の本数を計算する
// tsuruNoAshi : int -> int *
let tsuruNoAshi x = x * 2

// テスト 
let test1 = tsuruNoAshi 25 = 50
let test2 = tsuruNoAshi 28 = 56
let test3 = tsuruNoAshi 31 = 62

printfn "結果の表示" 
printfn "対象の関数: %A" tsuruNoAshi
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""
