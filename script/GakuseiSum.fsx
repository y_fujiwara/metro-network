(* GakuseiT list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはGakuseiT型 restが自己参照のケース)
*)

// 各学生のデータ (名前, 点数, 成績) を表す型
type GakuseiT = { 
    Namae : string;   // 学生の名前 
    Tensuu : int;     // 点数 
    Seiseki : string; // 成績
}

let gakusei1  = {Namae="asai"; Tensuu=70; Seiseki="?"}
let gakusei2  = {Namae="tanaka"; Tensuu=90; Seiseki="?"}
let gakusei3  = {Namae="yamada"; Tensuu=60; Seiseki="?"}
let gakusei4  = {Namae="yoshida"; Tensuu=80; Seiseki="?"}
let gakusei5  = {Namae="kimura"; Tensuu=85; Seiseki="?"}
let gakusei6  = {Namae="kudo"; Tensuu=65; Seiseki="?"}
let gakusei7  = {Namae="ito"; Tensuu=75; Seiseki="?"}

// 学生のリスト
let gakuseiList  = [
    gakusei1; gakusei2; gakusei3; gakusei4; gakusei5; gakusei6; gakusei7
    ]

// 目的: GakuseiTリストを受け取り全員の得点合計を求める
// gakuseiSum: GakuseiT list -> int
let gakuseiSum list =
    List.foldBack (fun g1 total -> g1.Tensuu + total) list 0

// テスト
let test1 = gakuseiSum gakuseiList = 525
let test2 = gakuseiSum [gakusei1;] = 70
let test3 = gakuseiSum [] = 0


printfn "結果の表示" 
printfn "対象の関数: %A" gakuseiSum
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test2: %b" test3
printfn ""





