// 目的: 2つのリストを受け取り、長さが同じか返す
// equalLength: 'a list -> 'b list -> bool
let rec equalLength list1 list2 = 
    match (list1, list2) with
      ([], []) -> true
    | ([], first2 :: rest2) -> false
    | (first1 :: rest1, []) -> false
    | (first1 :: rest1, first2 :: rest2) -> equalLength rest1 rest2

// テスト
let test1 = equalLength [] []
let test2 = not (equalLength [] [1; 3; 4;])
let test3 = not (equalLength [1; 2; 3;] [])
let test4 = not (equalLength ["1"; "2"; "3"] ["1"])
let test5 = not (equalLength [true; false] ["1"; "2"; "3"])
let test6 = equalLength [1.2; 2.2; 3.3; 4.5] [1.7; 4.1; 33.2; 34.4]

printfn "結果の表示" 
printfn "対象の関数: %A" equalLength
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn "test5: %b" test6
printfn ""

