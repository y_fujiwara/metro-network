(* PersonT list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはPersonT型 restが自己参照のケース)
*)

// 各人のデータ (名前, 身長m, 体重kg, 誕生日月日, 血液型) を表す型
type PersonT = {
    name: string;      // 名前
    sintyou: float;    // 身長
    taijyuu: float;    // 体重
    birthday: string;  // 誕生日
    bloodType: string; // 血液型
}

// 目的: PersonT型のリストと血液型を受け取り指定された血液型の総人数を求める
// countKetsueki: PersonT list -> int
let countKetsueki list blood =
    let rec countInner listInner ret =
        match listInner with
        | [] -> ret
        | {name = n; sintyou = s; taijyuu = t; birthday = b; bloodType = btype} :: rest
            -> if btype = blood then countInner rest (ret + 1) else countInner rest ret
    countInner list 0

// PersonT型のデータの例
let lst1 = []
let lst2 = [
    {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";};
    ]
let lst3 = [
    {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";};
    {name = "yoshiaki"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/08/30"; bloodType = "B";};
    ]
let lst4 = [
    {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/12/30"; bloodType = "B";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/20"; bloodType = "A";};
    ]
let lst5 = [
    {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/8/23"; bloodType = "A";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/8/22"; bloodType = "B";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/22"; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "AB";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "B";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "O";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "AB";}; 
    ]


// テスト
let test1 = countKetsueki lst1 "A" = 0
let test2 = countKetsueki lst2 "A" = 1
let test3 = countKetsueki lst3 "B" = 1
let test4 = countKetsueki lst4 "A" = 2
let test5 = countKetsueki lst5 "AB" = 2

printfn "結果の表示" 
printfn "対象の関数: %A" countKetsueki
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn ""


