(* PersonT2 list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはPersonT2型 restが自己参照のケース)
*)

// 各人のデータ (名前, 身長m, 体重kg, 誕生日月日, 血液型) を表す型
// 元々のデータ構造から少し変更した
type PersonT2 = {
    name: string;      // 名前
    sintyou: float;    // 身長
    taijyuu: float;    // 体重
    birthdayY: int;    // 誕生年
    birthdayM: int;    // 誕生月
    birthdayD: int;    // 誕生年
    bloodType: string; // 血液型
}

// PersonT型のデータの例
let lst1 = []
let lst2 = [
    {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthdayY = 1990; birthdayM = 6; birthdayD = 1; bloodType = "A";};
    ]
let lst3 = [
    {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthdayY = 1990; birthdayM = 6; birthdayD = 1; bloodType = "A";};
    {name = "yoshiaki"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 8; birthdayD = 30; bloodType = "B";};
    ]
let lst4 = [
    {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthdayY = 1990; birthdayM = 6; birthdayD = 1; bloodType = "A";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 12; birthdayD = 30; bloodType = "B";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 9; birthdayD = 20; bloodType = "A";};
    ]
let lst5 = [
    {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthdayY = 1990; birthdayM = 8; birthdayD = 23; bloodType = "A";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 8; birthdayD = 22; bloodType = "B";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 9; birthdayD = 22; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 9; birthdayD = 23; bloodType = "AB";}; 
    ]

// 目的: 入力されたリストから乙女座の人のみのリストを生成する
// otomeza: PersonT2 list -> PersonT2 list
let rec otomeza list = 
    match list with
        [] -> []
      | ({name = n; sintyou = s; taijyuu = t; birthdayY = y; birthdayM = m; birthdayD =d; bloodType = bd} as first) :: rest 
          -> if (m = 8 && d >= 23) || (m = 9 && d <= 22) then first :: otomeza rest else otomeza rest

// テスト
let test1 = otomeza lst1 = []
let test2 = otomeza lst2 = []
let test3 =
    otomeza lst3 = [
        {name = "yoshiaki"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 8; birthdayD = 30; bloodType = "B";};
        ]
let test4 =
    otomeza lst4 = [
        {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 9; birthdayD = 20; bloodType = "A";}; 
        ]
let test5 =
    otomeza lst5 = [
        {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthdayY = 1990; birthdayM = 8; birthdayD = 23; bloodType = "A";}; 
        {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthdayY = 2000; birthdayM = 9; birthdayD = 22; bloodType = "A";}; 
        ]

printfn "結果の表示" 
printfn "対象の関数: %A" otomeza
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn ""
