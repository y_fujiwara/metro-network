// 目的：入力された鶴と亀の数に応じて足の本数の合計を計算する
// tsuruKameNoAshi : int -> int -> int *
let tsuruKameNoAshi tsuru kame = tsuru * 2 + kame * 4

// テスト 
let test1 = tsuruKameNoAshi 25 25 = 25 * 2 + 25 * 4
let test2 = tsuruKameNoAshi 28 90 = 28 * 2 + 90 * 4
let test3 = tsuruKameNoAshi 31 362 = 31 * 2 + 362 * 4

printfn "結果の表示" 
printfn "対象の関数: %A" tsuruKameNoAshi
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""
