// 時給(円)
let jikyu = 950

// 基本給(円)
let kihonkyu = 100

// 優遇時給(円)
let yuguJiikyu = 980

// 目的：働いた時間xに応じたアルバイト代を計算する
// kyuyo : int -> int *
let kyuyo x = 
    kihonkyu + x * (if x < 30 then jikyu else yuguJiikyu)

// テスト 二つ目の等号は値が等しいかチェックするためのもの
let test1 = kyuyo 25 = 23850
let test2 = kyuyo 28 = 26700
let test3 = kyuyo 31 = 30480

printfn "結果の表示" 
printfn "対象の関数: %A" kyuyo
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""