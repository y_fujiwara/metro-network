// 目的: 受け取ったlstをクイックソートを使って昇順に整列する
// quickSort: int list -> int list
let rec quickSort lst =
    // 目的: lstの中からnよりpな要素のみを取り出す補助関数の抽象化
    // take: int -> int list -> (int -> int -> bool) -> int list
    let take n lst p = List.filter (fun item -> p item n) lst
    // 目的: lstの中からnより小さい要素のみを取り出す補助関数
    // takeLess: int -> int list -> int list
    let takeLess n lst = take n lst (<=)
    // 目的: lstの中からnより大きい要素のみを取り出す補助関数
    // takeGreater: int -> int list -> int list
    let takeGreater n lst =  take n lst (>)
    match lst with
    | [] -> []         // 解が自明な場合
    | first :: rest -> // それ以外
        quickSort (takeLess first rest) @ [first] @ quickSort (takeGreater first rest)

// テスト
let test1 = quickSort [] = []
let test2 = quickSort [1] = [1]
let test3 = quickSort [1; 2] = [1; 2]
let test4 = quickSort [2; 1] = [1; 2]
let test5 = quickSort [5; 4; 8; 8; 2; 3] = [2; 3; 4; 5; 8; 8]


printfn "結果の表示" 
printfn "対象の関数: %A" quickSort
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn ""
