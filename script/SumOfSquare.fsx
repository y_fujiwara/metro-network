// 目的：0から受け取った自然数までの二乗の和を求める
// sumOfSquare : int -> int
let rec sumOfSquare num =
    if num = 0 then 0 else (num * num) + sumOfSquare (num - 1)

let test1 = sumOfSquare 0 = 0
let test2 = sumOfSquare 1 = 1
let test3 = sumOfSquare 4 = 30

printfn "結果の表示" 
printfn "対象の関数: %A" sumOfSquare
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""

