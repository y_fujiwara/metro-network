// 目的：2次方程式の係数a,b,cを入力されたときに判別式の値を返す
//      但し、a,b,c∈R,a≠0とする
// hanbetsushiki : float -> float -> float -> float *
let hanbetsushiki a b c = b ** 2.0 - 4.0 * a * c

// テスト 
let test1 = hanbetsushiki 1.2 2.87 3.9 = 2.87 ** 2. - 4. * 1.2 * 3.9
let test2 = hanbetsushiki 3.4 298.3 18.78 = 298.3 ** 2. - 4.0 * 3.4 * 18.78
let test3 = hanbetsushiki 31.3 89.23 99.22 = 89.23 ** 2.0 - 4.0 * 31.3 * 99.22

printfn "結果の表示" 
printfn "対象の関数: %A" hanbetsushiki
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""