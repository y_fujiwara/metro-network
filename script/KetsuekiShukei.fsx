(* PersonT list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはPersonT型 restが自己参照のケース)
*)

// 各人のデータ (名前, 身長m, 体重kg, 誕生日月日, 血液型) を表す型
type PersonT = {
    name: string;      // 名前
    sintyou: float;    // 身長
    taijyuu: float;    // 体重
    birthday: string;  // 誕生日
    bloodType: string; // 血液型
}

// PersonT型のデータの例
let lst1 = []
let lst2 = [
    {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";};
    ]
let lst3 = [
    {name = "fujiwara"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";};
    {name = "yoshiaki"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/08/30"; bloodType = "B";};
    ]
let lst4 = [
    {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/06/01"; bloodType = "A";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/12/30"; bloodType = "B";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/20"; bloodType = "A";};
    ]
let lst5 = [
    {name = "aaa"; sintyou = 1.70; taijyuu = 67.8; birthday = "1990/8/23"; bloodType = "A";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/8/22"; bloodType = "B";};
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/22"; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "AB";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "A";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "B";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "O";}; 
    {name = "bbb"; sintyou = 2.10; taijyuu = 90.4; birthday = "2000/9/23"; bloodType = "AB";}; 
    ]

// 目的: 各血液型の人数がなんにいるかを組にして返す
// ketsuekiShukei: PersonT list -> (a, b, c, ab)
let rec ketsuekiShukei list = 
    match list with
      | [] -> (0, 0, 0, 0)
      | {name = n; sintyou = s; taijyuu = y; birthday = b; bloodType = bt} :: rest ->
          let (a, b, c, d) = ketsuekiShukei rest in
              if bt = "A" then (a + 1, b, c, d)
              else if bt = "B" then (a, b + 1, c, d)
              else if bt = "O" then (a, b, c + 1, d)
              else if bt = "AB" then (a, b, c, d + 1)
              else (a, b, c, d)

// テスト
let test1 = ketsuekiShukei lst1 = (0, 0, 0, 0)
let test2 = ketsuekiShukei lst2 = (1, 0, 0, 0)

let test3 = ketsuekiShukei lst3 = (1, 1, 0, 0)
let test4 = ketsuekiShukei lst4 = (2, 1, 0, 0)
let test5 = ketsuekiShukei lst5 = (4, 2, 1, 2)

printfn "結果の表示" 
printfn "対象の関数: %A" ketsuekiShukei
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn ""

