// 内部利用関数判別式
let hanbetsushiki a b c = b ** 2.0 - 4.0 * a * c

// 目的：2次方程式の係数a,b,cを入力されたときに虚数解の個数を返す
//      但し、a,b,c∈R,a≠0とする
// kyosuukai : float -> float -> float -> int *
let kyosuukai a b c = 
    if hanbetsushiki a b c < 0.0 then 2 else 0


// テスト 
let test1 = kyosuukai 1.2 2.87 3.9 = 2
let test2 = kyosuukai 3.4 298.3 18.78 = 0
let test3 = kyosuukai 1.0 4.0 4.0 = 0

printfn "結果の表示" 
printfn "対象の関数: %A" kyosuukai
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""
