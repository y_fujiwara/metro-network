(* GakuseiT list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはGakuseiT型 restが自己参照のケース)
*)

// 各学生のデータ (名前, 点数, 成績) を表す型
type GakuseiT = { 
    Namae : string;   // 学生の名前 
    Tensuu : int;     // 点数 
    Seiseki : string; // 成績
}

let gakusei1  = {Namae="asai"; Tensuu=70; Seiseki="?"}
let gakusei1' = {Namae="asai"; Tensuu=70; Seiseki="B"}
let gakusei2  = {Namae="tanaka"; Tensuu=90; Seiseki="?"}
let gakusei2' = {Namae="tanaka"; Tensuu=90; Seiseki="A"}
let gakusei3  = {Namae="yamada"; Tensuu=60; Seiseki="?"}
let gakusei3' = {Namae="yamada"; Tensuu=60; Seiseki="C"}
let gakusei4  = {Namae="yoshida"; Tensuu=80; Seiseki="?"}
let gakusei4' = {Namae="yoshida"; Tensuu=80; Seiseki="A"}
let gakusei5  = {Namae="kimura"; Tensuu=85; Seiseki="?"}
let gakusei5' = {Namae="kimura"; Tensuu=85; Seiseki="A"}
let gakusei6  = {Namae="kudo"; Tensuu=65; Seiseki="?"}
let gakusei6' = {Namae="kudo"; Tensuu=65; Seiseki="C"}
let gakusei7  = {Namae="ito"; Tensuu=75; Seiseki="?"}
let gakusei7' = {Namae="ito"; Tensuu=75; Seiseki="B"}
let gakusei8 = {Namae="dummy"; Tensuu=System.Int32.MinValue; Seiseki="X"}

// 学生のリスト
let gakuseiList  = [
    gakusei1; gakusei2; gakusei3; gakusei4; gakusei5; gakusei6; gakusei7
    ]

let gakuseiList' = [
    gakusei1'; gakusei2'; gakusei3'; gakusei4'; gakusei5'; gakusei6'; gakusei7'
    ]

// 目的: 与えられたリストをTennsuuフィールドが最大のものを返す
// gakuseiMax : GakuseiT list -> GakuseiT 
let rec gakuseiMax list =
    match list with
    | [] -> {Namae="dummy"; Tensuu=System.Int32.MinValue; Seiseki="X"}
    | first :: rest ->
        let maxGakusei = gakuseiMax rest in // 局所変数定義
        if maxGakusei.Tensuu <= first.Tensuu
        then first
        else maxGakusei

// テスト
let test1 = gakuseiMax gakuseiList = gakusei2
let test2 = gakuseiMax [gakusei1;] = gakusei1
let test3 = gakuseiMax [] = gakusei8


printfn "結果の表示" 
printfn "対象の関数: %A" gakuseiMax
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test2: %b" test3
printfn ""
