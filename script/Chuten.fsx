// 目的：入力された二次元座標に対して中点を返す
// taishoX : (float * float) -> (float * float) *
let chuten point1 point2 =
    match point1 with
        (x1, y1) -> match point2 with
            (x2, y2) -> ((x1 + x2) / 2.0, (y1 + y2) / 2.0)

// テスト 
let test1 = chuten (1.2, 2.87) (-1.2, 5.64) = ((1.2 + -1.2) / 2.0, (2.87 + 5.64) / 2.0)
let test2 = chuten (3.4, -298.3) (0.0, 100.8) = ((3.4 + 0.0) / 2.0, (-298.3 + 100.8) / 2.0)
let test3 = chuten (-1.0, -4.0) (-1.0, -4.0) = ((-1.0 + -1.0) / 2.0, (-4.0 + -4.0) / 2.0)

printfn "結果の表示" 
printfn "対象の関数: %A" chuten
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""


