(* int list は
     - []            空リスト、あるいは
     - first :: rest 最初の要素が first で残りのリストが rest
                     (firstはint型 restが自己参照のケース)
*)

// 目的: 受け取ったリストから偶数のみのリストを新たに作る
// even : int list -> int list
let rec even list =
    match list with
        [] -> []
      | first :: rest -> if first % 2 = 0 then first :: even rest else even rest

// テスト
let test1 = even [] = []
let test2 = even [1;] = []
let test3 = even [1; 2;] = [2]
let test4 = even [2; 1; 6; 4; 7;] = [2; 6; 4;]
let test5 = even (1 :: []) = []
let test6 = even (1 :: 2 :: []) = (2 :: [])


printfn "結果の表示" 
printfn "対象の関数: %A" even
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test4: %b" test4
printfn "test5: %b" test5
printfn "test6: %b" test6
printfn ""





