// 目的：入力された二次元座標に対してx軸について対称な座標を返す
// taishoX : (float * float) -> (float * float) *
let taishoX point =
    match point with
        (x, y) -> (x, -y)

// テスト 
let test1 = taishoX (1.2, 2.87) = (1.2, -2.87)
let test2 = taishoX (3.4, -298.3) = (3.4, 298.3)
let test3 = taishoX (-1.0, -4.0) = (-1.0, 4.0)

printfn "結果の表示" 
printfn "対象の関数: %A" taishoX
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""

