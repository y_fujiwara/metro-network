// 内部利用BMI計算関数
let bmi m kg = kg / (m ** 2.0)

// 目的：入力された体重と身長に従ってBMIを計算し、体型を返す
// taikei : float -> float -> string *
let taikei m kg = 
    if bmi m kg < 18.5 then "やせ"
        else if 18.5 <= bmi m kg && bmi m kg < 25.0 then "標準"
        else if 25.0 <= bmi m kg && bmi m kg < 30.0 then "肥満"
        else "高度肥満"

// テスト 
let test1 = taikei 1.7 67.0 = "標準"
let test2 = taikei 1.7 44.9 = "やせ"
let test3 = taikei 1.7 75.0 = "肥満"
let test4 = taikei 1.7 100.0 = "高度肥満"

printfn "結果の表示" 
printfn "対象の関数: %A" taikei
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn "test3: %b" test4
printfn ""

