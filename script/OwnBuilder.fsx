type OwnBuilder() = 
    member x.run(f) =
        printfn "実行 (%A)" f
        f ()

    member x.Return(r) =
        printfn "Return (%A)" r
        fun () -> r
    
    member x.Delay(f) =
        printfn "Delay (%A)" f
        fun () -> x.run(f () )

let ownBuilder = OwnBuilder()
let myExp = ownBuilder {
    let x = 6
    let y = 10
    return x.ToString() + y.ToString()
}

myExp

ownBuilder.run(myExp)