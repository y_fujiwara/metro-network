// 目的：入力された鶴と亀の合計数と足の合計本数から鶴の数を計算する
// 全部を鶴と仮定する方法で求める
// tsuruKameNoAshi : int -> int -> int *
let tsuruKame total totalLeg = total - (totalLeg - total * 2) / 2

// テスト 
let test1 = tsuruKame 50 150 = 25
let test2 = tsuruKame 118 416 = 28
let test3 = tsuruKame 393 1510 = 31

printfn "結果の表示" 
printfn "対象の関数: %A" tsuruKame
printfn "test1: %b" test1
printfn "test2: %b" test2
printfn "test3: %b" test3
printfn ""

